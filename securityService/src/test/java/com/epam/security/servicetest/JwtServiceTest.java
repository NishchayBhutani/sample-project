package com.epam.security.servicetest;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.security.Key;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.security.service.JwtService;

@ExtendWith(MockitoExtension.class)
class JwtServiceTest {

	@InjectMocks
	private JwtService jwtService;

	@Mock
	private Key mockKey;

	@Test
	void testGenerateToken() {
		String username = "testUser";
		String token = jwtService.generateToken(username);

		assertNotNull(token);
	}

	@Test
	void testValidateToken_ValidToken() {
		String token = jwtService.generateToken("testUser");

		assertDoesNotThrow(() -> jwtService.validateToken(token));
	}

	@Test
	void testValidateToken_InvalidToken() {
		String invalidToken = "invalid.token";

		assertThrows(Exception.class, () -> jwtService.validateToken(invalidToken));
	}

}
