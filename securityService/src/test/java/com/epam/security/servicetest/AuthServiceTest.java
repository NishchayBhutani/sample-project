package com.epam.security.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.security.dto.UserDTO;
import com.epam.security.entity.User;
import com.epam.security.repository.UserRepository;
import com.epam.security.service.AuthService;
import com.epam.security.service.JwtService;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {

	@InjectMocks
	private AuthService authService;

	@Mock
	private UserRepository userRepository;

	@Mock
	private PasswordEncoder passwordEncoder;

	@Mock
	private JwtService jwtService;

	@Mock
	private ModelMapper modelMapper;

	@Test
	void testSaveUser() {
		UserDTO userDTO = new UserDTO(1, "John", "Doe", "john@example.com", "password", true);
		User user = new User();
		when(passwordEncoder.encode(userDTO.getPassword())).thenReturn("encodedPassword");
		when(modelMapper.map(userDTO, User.class)).thenReturn(user);

		authService.saveUser(userDTO);

		verify(passwordEncoder).encode("password");
		verify(userRepository).save(user);
	}

	@Test
	void testGenerateToken() {
		String email = "john@example.com";
		when(jwtService.generateToken(email)).thenReturn("generatedToken");

		String token = authService.generateToken(email);

		assertEquals("generatedToken", token);
		verify(jwtService).generateToken(email);
	}

	@Test
	void testValidateToken() {
		String token = "validToken";
		doNothing().when(jwtService).validateToken(token);

		authService.validateToken(token);

		verify(jwtService).validateToken(token);
	}
}
