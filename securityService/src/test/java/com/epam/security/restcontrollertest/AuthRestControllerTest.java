package com.epam.security.restcontrollertest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.security.dto.UserDTO;
import com.epam.security.restcontroller.AuthRestController;
import com.epam.security.service.AuthService;

@WebMvcTest(AuthRestController.class)
@AutoConfigureMockMvc(addFilters = false)
class AuthRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AuthService authService;

	@MockBean
	private Authentication authentication;

	@MockBean
	private AuthenticationManager authenticationManager;

	@Test
    void testAddNewUser() throws Exception {
        when(authService.saveUser(any(UserDTO.class))).thenReturn("user added");
        mockMvc.perform(post("/auth/register")
                .contentType("application/json")
                .content("{ \"id\": 1, \"firstName\": \"John\", \"lastName\": \"Doe\", \"email\": \"john@example.com\", \"password\": \"password\", \"isActive\": true }"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value("user added"));
    }

	@Test
	void testGetToken() throws Exception {
		when(authentication.isAuthenticated()).thenReturn(true);
		when(authenticationManager.authenticate(any(Authentication.class))).thenReturn(authentication);
		when(authService.generateToken("john@example.com")).thenReturn("generated-token");

		mockMvc.perform(post("/auth/token").contentType("application/json")
				.content("{ \"email\": \"john@example.com\", \"password\": \"password\" }")).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("generated-token"));
	}

	@Test
	void testValidateToken() throws Exception {
		String token = "valid-token";
		mockMvc.perform(get("/auth/validate").param("token", token)).andExpect(status().isOk())
				.andExpect(jsonPath("$").value("token is valid"));
	}

}
