package com.epam.security.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.security.dto.UserDTO;
import com.epam.security.entity.User;
import com.epam.security.repository.UserRepository;

@Service
public class AuthService {

	@Autowired
	private UserRepository userCredentialRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtService jwtService;

	@Autowired
	private ModelMapper modelMapper;

	public String saveUser(UserDTO userDTO) {
		userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
		userCredentialRepository.save(modelMapper.map(userDTO, User.class));
		return "user added";
	}

	public String generateToken(String email) {
		return jwtService.generateToken(email);
	}

	public void validateToken(String token) {
		jwtService.validateToken(token);
	}
}
