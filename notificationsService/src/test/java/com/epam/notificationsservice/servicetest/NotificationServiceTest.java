package com.epam.notificationsservice.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.notificationservice.utility.MessageMapper;
import com.epam.notificationsservice.dao.Notification;
import com.epam.notificationsservice.repo.NotificationRepository;
import com.epam.notificationsservice.service.NotificationService;

@ExtendWith(MockitoExtension.class)
class NotificationServiceTest {

	@InjectMocks
	NotificationService notificationService;

	@Mock
	NotificationRepository notificationRepository;

	@Mock
	JavaMailSender mailSender;

	@Mock
	ModelMapper modelMapper;

	NotificationDTO notificationDTO;

	@BeforeEach
	void setUp() {
		notificationDTO = new NotificationDTO("to@gmail.com", "cc@gmail.com", "from@gmail.com", new HashMap<>(),
				"REGISTRATION");
	}

	@Test
	void TestSendNotification() {
		Notification notification = MessageMapper.createNotification(notificationDTO);
		System.out.println(notification.getCc());
		doNothing().when(mailSender).send(any(SimpleMailMessage.class));
		when(notificationRepository.save(notification)).thenReturn(notification);
		Notification result = notificationService.sendNotification(notificationDTO);
		assertEquals(notification, result);
		verify(mailSender).send(any(SimpleMailMessage.class));
		verify(notificationRepository).save(notification);
	}

	@Test
	void testSendNotificationNoCc() {
		NotificationDTO newNotificationDTO = new NotificationDTO("to@gmail.com", null, "from@gmail.com",
				new HashMap<>(), "REGISTRATION");
		Notification notification = MessageMapper.createNotification(newNotificationDTO);
		doNothing().when(mailSender).send(any(SimpleMailMessage.class));
		when(notificationRepository.save(notification)).thenReturn(notification);
		Notification result = notificationService.sendNotification(newNotificationDTO);
		assertEquals(notification, result);
		verify(mailSender).send(any(SimpleMailMessage.class));
		verify(notificationRepository).save(notification);
	}

	@Test
	void testSendNotificationFailure() {
		doThrow(new MailSendException("Failed to send mail")).when(mailSender).send(any(SimpleMailMessage.class));
		Notification notification = MessageMapper.createNotification(notificationDTO);
		notification.setStatus("failure");
		notification.setRemarks("Failed to send mail");
		when(notificationRepository.save(notification)).thenReturn(notification);
		Notification result = notificationService.sendNotification(notificationDTO);
		verify(mailSender).send(any(SimpleMailMessage.class));
		assertEquals("failure", result.getStatus());
		assertNotNull(result.getRemarks());
	}
}
