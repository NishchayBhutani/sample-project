package com.epam.mainservice.requestdata;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDTO {
	private String to;
	private String cc;
	private String from;
	private Map<String, String> parameters;
	private String emailType;
}
