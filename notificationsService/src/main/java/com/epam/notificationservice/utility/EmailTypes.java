package com.epam.notificationservice.utility;

public enum EmailTypes {
	REGISTRATION("user successfully registered"), TRAINEE_UPDATION("trainee successfully updated"),
	TRAINER_UPDATION("trainer successfully updated"), TRAINING_CREATION("new training added");

	private final String value;

	EmailTypes(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
