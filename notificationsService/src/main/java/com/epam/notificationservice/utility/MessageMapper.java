package com.epam.notificationservice.utility;

import java.util.Map;
import java.util.stream.Collectors;

import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.notificationsservice.dao.Notification;

public class MessageMapper {

	public static Notification createNotification(NotificationDTO notificationDTO) {
		Notification notification = new Notification();
		Map<String, String> parameters = notificationDTO.getParameters();
		notification.setId(notificationDTO.getTo());
		notification.setTo(notificationDTO.getTo());
		notification.setFrom(notificationDTO.getFrom());
		if(notificationDTO.getCc()!=null) {
			notification.setCc(notificationDTO.getCc());
		}
		notification.setSubject(EmailTypes.valueOf(notificationDTO.getEmailType()).getValue());
		notification.setBody(parameters.entrySet().stream().map(entry -> entry.getKey() + ": " + entry.getValue())
				.collect(Collectors.joining("\n")));
		notification.setStatus("success");
		notification.setRemarks("none");
		return notification;
	}
}
