package com.epam.notificationsservice.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.notificationservice.utility.MessageMapper;
import com.epam.notificationsservice.dao.Notification;
import com.epam.notificationsservice.repo.NotificationRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationService {

	@Autowired
	NotificationRepository notificationRepository;

	@Autowired
	JavaMailSender mailSender;

	@Autowired
	ModelMapper modelMapper;

	public Notification sendNotification(NotificationDTO notificationDTO) {
		Notification notification = MessageMapper.createNotification(notificationDTO);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(notification.getTo());
		mailMessage.setFrom(notification.getFrom());
		if (notification.getCc() != null) {
			mailMessage.setCc(notification.getCc());
		}
		mailMessage.setSubject(notification.getSubject());
		mailMessage.setText(notification.getBody());
		try {
			mailSender.send(mailMessage);
			log.info("email sent to {}", notificationDTO.getTo());
		} catch (MailException e) {
			notification.setStatus("failure");
			log.info("could not send email to {}", notificationDTO.getTo());
			notification.setRemarks(e.getMessage());
		}
		return notificationRepository.save(notification);
	}
}
