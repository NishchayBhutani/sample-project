package com.epam.notificationsservice.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.epam.notificationsservice.dao.Notification;

public interface NotificationRepository extends MongoRepository<Notification, Integer>{

}
