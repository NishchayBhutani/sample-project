package com.epam.notificationsservice.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.notificationsservice.service.NotificationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationConsumer {

	@Autowired
	NotificationService notificationService;

	@KafkaListener(topics = "notifications", groupId = "myGroup")
	public void consumer(NotificationDTO notificationDTO) {

		log.info("message received : " + notificationDTO);
		notificationService.sendNotification(notificationDTO);
	}
}
