package com.epam.notificationsservice.dao;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Document(collection = "notifications")
public class Notification {
	@Id
	private String id;
	@Email
	@NotBlank
	private String from;
	@Email
	@NotBlank
	private String to;
	@Email
	private String cc;
	@NotBlank
	private String subject;
	@NotBlank
	private String body;
	@NotBlank
	private String status;
	private String remarks;
}
