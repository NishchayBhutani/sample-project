package com.epam.gatewayforgymapp.filter;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;

@Component
public class RouteValidator {

	public static final List<String> openApiEndpoints = List.of("/eureka", "/auth/validate");

	public static final List<String> registerEndPoints = List.of("/gymapp/trainers", "/gymapp/trainees", "/auth/register");

	public static final List<String> loginEndPoint = List.of("/auth/token");

	public final Predicate<ServerHttpRequest> isLogin = request -> loginEndPoint.stream()
			.anyMatch(uri -> request.getURI().getPath().contains(uri));

	public final Predicate<ServerHttpRequest> isSecured = request -> openApiEndpoints.stream()
			.noneMatch(uri -> request.getURI().getPath().contains(uri))
			&& registerEndPoints.stream().noneMatch(uri -> request.getURI().getPath().endsWith(uri));

}