package com.epam.mainservice.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.mainservice.responsedata.TrainerDetailsResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TrainerDetailsProducer {

	@Autowired
	private KafkaTemplate<String, TrainerDetailsResponse> kafkaTemplate;

//	public void sendTrainerDetails(List<TrainerTrainingsListResponse> trainerTrainingsListResponse,
//			TrainerProfileResponse trainerProfileResponse) {
//		log.info("details sent : " + trainerTrainingsListResponse);
//		kafkaTemplate.send("trainer-details", TrainerDetailsResponse.builder().trainerTrainingsList(trainerTrainingsListResponse).fi);
//	}
	public void sendTrainerDetails(TrainerDetailsResponse trainerDetailsResponse) {
		log.info("details sent : " + trainerDetailsResponse);
		kafkaTemplate.send("trainer-details", trainerDetailsResponse);
	}

}
