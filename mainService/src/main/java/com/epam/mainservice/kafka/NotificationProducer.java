package com.epam.mainservice.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.epam.mainservice.requestdata.NotificationDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NotificationProducer {
	@Autowired
	private KafkaTemplate<String, NotificationDTO> kafkaTemplate;

	public void sendNotification(NotificationDTO notificationDTO) {
		log.info("email sent : " + notificationDTO);
		kafkaTemplate.send("notifications", notificationDTO);
	}
}
