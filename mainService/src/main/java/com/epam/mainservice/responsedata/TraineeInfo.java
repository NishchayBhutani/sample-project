package com.epam.mainservice.responsedata;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TraineeInfo {
	private String email;
	private String firstName;
	private String lastName;
}
