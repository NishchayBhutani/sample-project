package com.epam.mainservice.responsedata;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserCreationResponse {
	private String email;
	private String password;
}
