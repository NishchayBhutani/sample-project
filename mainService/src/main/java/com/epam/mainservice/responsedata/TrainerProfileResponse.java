package com.epam.mainservice.responsedata;

import java.util.List;

import com.epam.mainservice.dao.TrainingType;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TrainerProfileResponse {
	private String email;
	private String firstName;
	private String lastName;
	private TrainingType trainingType;
	private boolean isActive;
	private List<TraineeInfo> traineesList;
}
