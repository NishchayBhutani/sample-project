package com.epam.mainservice.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.mainservice.dao.TrainingType;

public interface TrainingTypeRepository extends JpaRepository<TrainingType, Integer>{
	public Optional<TrainingType> findByTrainingTypeName(String trainingTypeName);
}
