package com.epam.mainservice.repo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.epam.mainservice.dao.Trainer;
import com.epam.mainservice.dao.Training;

public interface TrainerRepository extends JpaRepository<Trainer, Integer>{
	public Optional<Trainer> findByUserEmail(String email);
	@Query("SELECT t FROM Training t WHERE (:from IS NULL OR t.trainingDate >= :from) AND (:to IS NULL OR t.trainingDate < :to) AND t.trainer = :trainer")
	List<Training> findAllTrainingInBetween(@Param("from") LocalDate from, @Param("to") LocalDate to,
			@Param("trainer") Trainer trainer);
}
