package com.epam.mainservice.repo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.epam.mainservice.dao.Trainee;
import com.epam.mainservice.dao.Training;

public interface TraineeRepository extends JpaRepository<Trainee, Integer> {
	public Optional<Trainee> findByUserEmail(String email);

	public void deleteByUserEmail(String email);

	@Query("SELECT t FROM Training t WHERE (:from IS NULL OR t.trainingDate >= :from) AND (:to IS NULL OR t.trainingDate < :to) AND t.trainee = :trainee")
	List<Training> findAllTrainingInBetween(@Param("from") LocalDate from, @Param("to") LocalDate to,
			@Param("trainee") Trainee trainee);
}
