package com.epam.mainservice.requestdata;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class LoginChangeRequest {
	@Email
	@NotBlank
	private String email;
	@NotBlank
	private String oldPassword;
	@NotBlank
	private String newPassword;
}
