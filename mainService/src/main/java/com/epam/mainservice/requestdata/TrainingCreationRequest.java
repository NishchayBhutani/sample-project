package com.epam.mainservice.requestdata;

import java.time.LocalDate;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TrainingCreationRequest {
	@Email
	@NotBlank
	private String traineeEmail;
	@Email
	@NotBlank
	private String trainerEmail;
	@NotBlank
	private String trainingName;
	@NotNull
	private LocalDate trainingDate;
	@Min(value = 1)
	private int trainingDuration;
}
