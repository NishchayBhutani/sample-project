package com.epam.mainservice.requestdata;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TraineeTrainingsListRequest {
	private LocalDate from;
	private LocalDate to;
	private String trainerName;
	private String trainingType;
}
