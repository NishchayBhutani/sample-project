package com.epam.mainservice.requestdata;

import java.util.List;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class TrainerEmailListRequest {
	@NotEmpty
	private List<String> trainerEmailList;
}
