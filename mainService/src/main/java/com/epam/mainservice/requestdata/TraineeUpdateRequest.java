package com.epam.mainservice.requestdata;

import java.time.LocalDate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TraineeUpdateRequest {
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@NotNull
	private LocalDate dob;
	@NotBlank
	private String address;
	private boolean isActive;
}
