package com.epam.mainservice.requestdata;

import java.time.LocalDate;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class TraineeCreationRequest {
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	@Email
	@NotBlank	
	private String email;
	private LocalDate dob;
	private String address;
}
