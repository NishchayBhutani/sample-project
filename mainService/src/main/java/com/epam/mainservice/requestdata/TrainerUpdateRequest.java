package com.epam.mainservice.requestdata;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class TrainerUpdateRequest {
	@NotBlank
	private String firstName;
	@NotBlank
	private String lastName;
	private boolean isActive;	
}
