package com.epam.mainservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.mainservice.dao.Trainee;
import com.epam.mainservice.dao.Trainer;
import com.epam.mainservice.dao.Training;
import com.epam.mainservice.exception.TrainingException;
import com.epam.mainservice.kafka.NotificationProducer;
import com.epam.mainservice.kafka.TrainerDetailsProducer;
import com.epam.mainservice.repo.TraineeRepository;
import com.epam.mainservice.repo.TrainerRepository;
import com.epam.mainservice.repo.TrainingRepository;
import com.epam.mainservice.repo.TrainingTypeRepository;
import com.epam.mainservice.requestdata.TrainerTrainingsListRequest;
import com.epam.mainservice.requestdata.TrainingCreationRequest;
import com.epam.mainservice.responsedata.TrainerDetailsResponse;
import com.epam.mainservice.utility.MessageMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TrainingService {

	@Autowired
	TrainingRepository trainingRepository;

	@Autowired
	TraineeRepository traineeRepository;

	@Autowired
	TrainerRepository trainerRepository;

	@Autowired
	TrainingTypeRepository trainingTypeRepository;

	@Autowired
	NotificationProducer notificationProducer;

	@Autowired
	TrainerDetailsProducer trainerDetailsProducer;

	@Autowired
	TrainerService trainerService;

	public void create(TrainingCreationRequest trainingCreationRequest) {
		Trainer trainer = trainerRepository.findByUserEmail(trainingCreationRequest.getTrainerEmail())
				.orElseThrow(() -> new TrainingException(
						"trainer not found for email : " + trainingCreationRequest.getTrainerEmail()));
		Trainee trainee = traineeRepository.findByUserEmail(trainingCreationRequest.getTraineeEmail())
				.orElseThrow(() -> new TrainingException(
						"trainee not found for email : " + trainingCreationRequest.getTraineeEmail()));
		if (trainer.getTraineeList().contains(trainee)) {
			throw new TrainingException("trainer already associated with trainee");
		}
		Training training = Training.builder().trainee(trainee).trainer(trainer)
				.trainingName(trainingCreationRequest.getTrainingName())
				.trainingDate(trainingCreationRequest.getTrainingDate()).trainingType(trainer.getTrainingType())
				.trainingDuration(trainingCreationRequest.getTrainingDuration()).build();
		trainer.getTraineeList().add(trainee);
		trainer.getTrainingsList().add(training);
		trainee.getTrainersList().add(trainer);
		trainee.getTrainingsList().add(training);
		trainingRepository.save(training);
		log.info("training created");
		notificationProducer.sendNotification(MessageMapper.getNotificationDTO(training));
		TrainerDetailsResponse trainerDetailsResponse = TrainerDetailsResponse.builder()
				.email(trainer.getUser().getEmail()).firstName(trainer.getUser().getFirstName())
				.lastName(trainer.getUser().getLastName()).isActive(trainer.getUser().isActive())
				.trainerTrainingsList(trainerService.getTrainingsList(trainer.getUser().getEmail(),
						new TrainerTrainingsListRequest()))
				.build();
		trainerDetailsProducer.sendTrainerDetails(trainerDetailsResponse);
	}

}
