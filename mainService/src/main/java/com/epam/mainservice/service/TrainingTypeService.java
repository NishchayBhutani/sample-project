package com.epam.mainservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.repo.TrainingTypeRepository;

@Service
public class TrainingTypeService {

	@Autowired
	public TrainingTypeRepository trainingTypeRepository;
	
	public List<TrainingType> getAll() {
		return trainingTypeRepository.findAll();
	}
	
}
