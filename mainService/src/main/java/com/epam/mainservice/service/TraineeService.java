package com.epam.mainservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.mainservice.dao.Trainee;
import com.epam.mainservice.dao.Trainer;
import com.epam.mainservice.dao.Training;
import com.epam.mainservice.dao.User;
import com.epam.mainservice.exception.TraineeException;
import com.epam.mainservice.kafka.NotificationProducer;
import com.epam.mainservice.repo.TraineeRepository;
import com.epam.mainservice.repo.TrainerRepository;
import com.epam.mainservice.repo.TrainingTypeRepository;
import com.epam.mainservice.requestdata.TraineeCreationRequest;
import com.epam.mainservice.requestdata.TraineeTrainingsListRequest;
import com.epam.mainservice.requestdata.TraineeUpdateRequest;
import com.epam.mainservice.requestdata.TrainerEmailListRequest;
import com.epam.mainservice.responsedata.TraineeProfileResponse;
import com.epam.mainservice.responsedata.TraineeTrainingsListResponse;
import com.epam.mainservice.responsedata.TrainerInfo;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.utility.MessageMapper;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TraineeService {

	@Autowired
	TraineeRepository traineeRepository;

	@Autowired
	TrainerRepository trainerRepository;

	@Autowired
	TrainingTypeRepository trainingTypeRepository;

	@Autowired
	UserService userService;

	@Autowired
	NotificationProducer notificationProducer;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Value("${app.notification.from}")
	private String notificationFrom;

	private static final String NOT_FOUND = "trainee not found";

	public UserCreationResponse create(TraineeCreationRequest traineeCreationRequest) {
		String randomPassword = UUID.randomUUID().toString();
		User user = User.builder().firstName(traineeCreationRequest.getFirstName())
				.lastName(traineeCreationRequest.getLastName()).email(traineeCreationRequest.getEmail())
				.password(passwordEncoder.encode(randomPassword)).isActive(true).build();
		userService.create(user);
		Trainee trainee = Trainee.builder().user(user).dob(traineeCreationRequest.getDob())
				.address(traineeCreationRequest.getAddress()).build();
		traineeRepository.save(trainee);
		log.info("trainee created");
		UserCreationResponse userCreationResponse = new UserCreationResponse(user.getEmail(), randomPassword);
		notificationProducer.sendNotification(MessageMapper.getNotificationDTO(userCreationResponse));
		return userCreationResponse;
	}

	public TraineeProfileResponse getProfile(String email) {
		Trainee trainee = traineeRepository.findByUserEmail(email).orElseThrow(() -> new TraineeException(NOT_FOUND));
		List<TrainerInfo> trainerInfoList = new ArrayList<>();
		List<Trainer> trainersList = trainee.getTrainersList();
		for (Trainer trainer : trainersList) {
			trainerInfoList.add(new TrainerInfo(trainer.getUser().getEmail(), trainer.getUser().getFirstName(),
					trainer.getUser().getLastName(), trainer.getTrainingType()));
		}
		return new TraineeProfileResponse(email, trainee.getUser().getFirstName(), trainee.getUser().getLastName(),
				trainee.getDob(), trainee.getAddress(), trainee.getUser().isActive(), trainerInfoList);
	}

	public TraineeProfileResponse updateProfile(String email, TraineeUpdateRequest traineeUpdateRequest) {
		Trainee trainee = traineeRepository.findByUserEmail(email).orElseThrow(() -> new TraineeException(NOT_FOUND));
		User user = trainee.getUser();
		user.setFirstName(traineeUpdateRequest.getFirstName());
		user.setLastName(traineeUpdateRequest.getLastName());
		user.setActive(traineeUpdateRequest.isActive());
		trainee.setDob(traineeUpdateRequest.getDob());
		trainee.setAddress(traineeUpdateRequest.getAddress());
		traineeRepository.save(trainee);
		log.info("trainee profile updated");
		List<TrainerInfo> trainerInfoList = new ArrayList<>();
		List<Trainer> trainersList = trainee.getTrainersList();
		for (Trainer trainer : trainersList) {
			trainerInfoList.add(new TrainerInfo(trainer.getUser().getEmail(), trainer.getUser().getFirstName(),
					trainer.getUser().getLastName(), trainer.getTrainingType()));
		}
		TraineeProfileResponse traineeProfileResponse = new TraineeProfileResponse(email,
				trainee.getUser().getFirstName(), trainee.getUser().getLastName(), trainee.getDob(),
				trainee.getAddress(), trainee.getUser().isActive(), trainerInfoList);
		notificationProducer.sendNotification(MessageMapper.getNotificationDTO(traineeProfileResponse));
		return traineeProfileResponse;
	}

	public TraineeProfileResponse updateTrainersList(String email, TrainerEmailListRequest trainerEmailListRequest) {
		Trainee trainee = traineeRepository.findByUserEmail(email).orElseThrow(() -> new TraineeException(NOT_FOUND));
		List<Trainer> trainersList = new ArrayList<>();
		for (String trainerEmail : trainerEmailListRequest.getTrainerEmailList()) {
			trainersList.add(trainerRepository.findByUserEmail(trainerEmail)
					.orElseThrow(() -> new TraineeException("one or more trainers not found")));
		}
		trainee.setTrainersList(trainersList);
		traineeRepository.save(trainee);
		log.info("trainee's trainers list updated");
		List<TrainerInfo> trainerInfoList = new ArrayList<>();
		for (Trainer trainer : trainersList) {
			trainerInfoList.add(new TrainerInfo(trainer.getUser().getEmail(), trainer.getUser().getFirstName(),
					trainer.getUser().getLastName(), trainer.getTrainingType()));
		}
		return new TraineeProfileResponse(email, trainee.getUser().getFirstName(), trainee.getUser().getLastName(),
				trainee.getDob(), trainee.getAddress(), trainee.getUser().isActive(), trainerInfoList);
	}

	@Transactional
	public void deleteProfile(String email) {
		traineeRepository.deleteByUserEmail(email);
		log.info("trainee profile deleted");
	}

	public List<TraineeTrainingsListResponse> getTrainingsList(String email,
			TraineeTrainingsListRequest traineeTrainingsListRequest) {
		Trainee trainee = traineeRepository.findByUserEmail(email).orElseThrow(() -> new TraineeException(NOT_FOUND));
		List<TraineeTrainingsListResponse> traineeTrainingsResponseList = new ArrayList<>();
		List<Training> filteredTrainingsList = traineeRepository.findAllTrainingInBetween(
				traineeTrainingsListRequest.getFrom(), traineeTrainingsListRequest.getTo(), trainee);
		filteredTrainingsList = filteredTrainingsList.stream()
				.filter(training -> traineeTrainingsListRequest.getTrainerName() == null || training.getTrainer()
						.getUser().getFirstName().equals(traineeTrainingsListRequest.getTrainerName()))
				.filter(training -> traineeTrainingsListRequest.getTrainingType() == null || training.getTrainingType()
						.getTrainingTypeName().equals(traineeTrainingsListRequest.getTrainingType()))
				.toList();
		for (Training training : filteredTrainingsList) {
			traineeTrainingsResponseList.add(new TraineeTrainingsListResponse(training.getTrainingName(),
					training.getTrainingDate(), training.getTrainingType(), training.getTrainingDuration(),
					training.getTrainer().getUser().getFirstName()));
		}
		return traineeTrainingsResponseList;
	}

	public List<TrainerInfo> getUnassociatedTrainersList(String email) {
		List<Trainer> associatedTrainersList = traineeRepository.findByUserEmail(email)
				.orElseThrow(() -> new TraineeException(NOT_FOUND)).getTrainersList();
		List<Trainer> unassociatedTrainersList = trainerRepository.findAll().stream()
				.filter(trainer -> !associatedTrainersList.contains(trainer)).toList();
		List<TrainerInfo> trainerInfoList = new ArrayList<>();
		for (Trainer trainer : unassociatedTrainersList) {
			trainerInfoList.add(new TrainerInfo(trainer.getUser().getEmail(), trainer.getUser().getFirstName(),
					trainer.getUser().getLastName(), trainer.getTrainingType()));
		}
		return trainerInfoList;
	}
}
