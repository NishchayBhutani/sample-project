package com.epam.mainservice.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.mainservice.dao.User;
import com.epam.mainservice.exception.UserException;
import com.epam.mainservice.repo.UserRepository;
import com.epam.mainservice.requestdata.LoginChangeRequest;
import com.epam.mainservice.requestdata.LoginRequest;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	ModelMapper modelMapper;

	private static final String NOT_FOUND = "user not found";

	public User create(User user) {
		return userRepository.save(user);
	}

	public void login(LoginRequest loginRequest) {
		if (!userRepository.existsByEmailAndPassword(loginRequest.getEmail(), loginRequest.getPassword())) {
			throw new UserException(NOT_FOUND);
		}
	}

	public void changeLogin(LoginChangeRequest loginChangeRequest) {
		User user = userRepository
				.findByEmailAndPassword(loginChangeRequest.getEmail(), loginChangeRequest.getOldPassword())
				.orElseThrow(() -> new UserException(NOT_FOUND));
		user.setPassword(loginChangeRequest.getNewPassword());
		userRepository.save(user);
	}
}
