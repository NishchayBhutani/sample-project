package com.epam.mainservice.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.mainservice.requestdata.LoginChangeRequest;
import com.epam.mainservice.requestdata.LoginRequest;
import com.epam.mainservice.service.UserService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("gymapp/users")
public class UserRestController {

	@Autowired
	UserService userService;

	@PostMapping
	public ResponseEntity<Void> loginUser(@RequestBody @Valid LoginRequest loginData) {
		log.info("post request for user login for email : {}", loginData.getEmail());
		userService.login(loginData);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Void> changeUserLogin(@RequestBody @Valid LoginChangeRequest loginChangeData) {
		log.info("put request for chaning user login for email : {}", loginChangeData.getEmail());
		userService.changeLogin(loginChangeData);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
