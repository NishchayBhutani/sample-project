package com.epam.mainservice.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.mainservice.requestdata.TrainerCreationRequest;
import com.epam.mainservice.requestdata.TrainerTrainingsListRequest;
import com.epam.mainservice.requestdata.TrainerUpdateRequest;
import com.epam.mainservice.responsedata.TrainerProfileResponse;
import com.epam.mainservice.responsedata.TrainerTrainingsListResponse;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.service.TrainerService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("gymapp/trainers")
public class TrainerRestController {

	@Autowired
	TrainerService trainerService;

	@PostMapping
	public ResponseEntity<UserCreationResponse> createTrainer(@RequestBody @Valid TrainerCreationRequest requestData) {
		log.info("post request for creating trainer");
		return new ResponseEntity<>(trainerService.register(requestData), HttpStatus.CREATED);
	}

	@GetMapping("{email}")
	public ResponseEntity<TrainerProfileResponse> getTrainerProfile(@PathVariable @Valid @Email String email) {
		log.info("get request for getting trainer profile for email : {}", email);
		return new ResponseEntity<>(trainerService.getProfile(email), HttpStatus.OK);
	}

	@PutMapping("{email}")
	public ResponseEntity<TrainerProfileResponse> updateTrainerProfile(@PathVariable @Valid @Email String email,
			@RequestBody @Valid TrainerUpdateRequest trainerUpdateRequest) {
		log.info("put request for updating trainer profile for email : {}", email);
		return new ResponseEntity<>(trainerService.updateProfile(email, trainerUpdateRequest), HttpStatus.OK);
	}

	@GetMapping("{email}/trainings")
	public ResponseEntity<List<TrainerTrainingsListResponse>> getTrainerTrainings(
			@PathVariable @Valid @Email String email,
			@RequestBody(required = false) @Valid TrainerTrainingsListRequest trainerTrainingsListRequest) {
		log.info("get request for getting trainer's trainings list for email : {}", email);
		return new ResponseEntity<>(trainerService.getTrainingsList(email, trainerTrainingsListRequest), HttpStatus.OK);
	}
}
