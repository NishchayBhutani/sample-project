package com.epam.mainservice.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.mainservice.requestdata.TrainingCreationRequest;
import com.epam.mainservice.service.TrainingService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("gymapp/trainings")
public class TrainingRestController {

	@Autowired
	TrainingService trainingService;

	@PostMapping
	public ResponseEntity<Void> createTraining(@RequestBody @Valid TrainingCreationRequest trainingRequestData) {
		trainingService.create(trainingRequestData);
		log.info("post request for creating training");
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
