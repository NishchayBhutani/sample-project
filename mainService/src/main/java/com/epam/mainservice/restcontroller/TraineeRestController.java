package com.epam.mainservice.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.mainservice.requestdata.TraineeCreationRequest;
import com.epam.mainservice.requestdata.TraineeTrainingsListRequest;
import com.epam.mainservice.requestdata.TraineeUpdateRequest;
import com.epam.mainservice.requestdata.TrainerEmailListRequest;
import com.epam.mainservice.responsedata.TraineeProfileResponse;
import com.epam.mainservice.responsedata.TraineeTrainingsListResponse;
import com.epam.mainservice.responsedata.TrainerInfo;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.service.TraineeService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("gymapp/trainees")
public class TraineeRestController {

	@Autowired
	TraineeService traineeService;

	@PostMapping
	public ResponseEntity<UserCreationResponse> createTrainee(
			@RequestBody @Valid TraineeCreationRequest traineeRequestData) {
		log.info("post request for creating trainee");
		return new ResponseEntity<>(traineeService.create(traineeRequestData), HttpStatus.CREATED);
	}

	@GetMapping("{email}")
	public ResponseEntity<TraineeProfileResponse> getTraineeProfile(@PathVariable @Valid @Email String email) {
		log.info("get request for getting trainee's profile for email : {}", email);
		return new ResponseEntity<>(traineeService.getProfile(email), HttpStatus.OK);
	}

	@PutMapping("{email}")
	public ResponseEntity<TraineeProfileResponse> updateTraineeProfile(@PathVariable @Valid @Email String email,
			@RequestBody @Valid TraineeUpdateRequest traineeUpdateRequest) {
		log.info("put request for updating trainee's profile for email : {}", email);
		return new ResponseEntity<>(traineeService.updateProfile(email, traineeUpdateRequest), HttpStatus.OK);
	}

	@DeleteMapping("{email}")
	public ResponseEntity<Void> deleteTraineeProfile(@PathVariable @Valid @Email String email) {
		log.info("delete request for deleting trainee's profile for email : {}", email);
		traineeService.deleteProfile(email);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("{email}/trainers")
	public ResponseEntity<List<TrainerInfo>> getUnassociatedTraineeTrainers(@PathVariable @Valid @Email String email) {
		log.info("get request for getting trainers unassociated with trainee for email : {}", email);
		return new ResponseEntity<>(traineeService.getUnassociatedTrainersList(email), HttpStatus.OK);
	}

	@PutMapping("{email}/trainers")
	public ResponseEntity<TraineeProfileResponse> updateTraineeTrainers(@PathVariable @Valid @Email String email,
			@RequestBody @Valid TrainerEmailListRequest trainerEmailListRequest) {
		log.info("put request for updating trainee's trainer list for email : {}", email);
		return new ResponseEntity<>(traineeService.updateTrainersList(email, trainerEmailListRequest), HttpStatus.OK);
	}

	@GetMapping("{email}/trainings")
	public ResponseEntity<List<TraineeTrainingsListResponse>> getTraineeTrainings(
			@PathVariable @Valid @Email String email,
			@RequestBody(required = false) @Valid TraineeTrainingsListRequest traineeTrainingsListRequest) {
		log.info("get request for getting trainee's trainings list for email : {}", email);
		return new ResponseEntity<>(traineeService.getTrainingsList(email, traineeTrainingsListRequest), HttpStatus.OK);
	}

}
