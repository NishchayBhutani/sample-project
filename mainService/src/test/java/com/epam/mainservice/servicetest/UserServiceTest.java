package com.epam.mainservice.servicetest;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.mainservice.dao.User;
import com.epam.mainservice.exception.UserException;
import com.epam.mainservice.repo.UserRepository;
import com.epam.mainservice.requestdata.LoginChangeRequest;
import com.epam.mainservice.requestdata.LoginRequest;
import com.epam.mainservice.service.UserService;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

	@InjectMocks
	UserService userService;

	@Mock
	UserRepository userRepository;

	@Mock
	ModelMapper modelMapper;

	@Test
	void testCreateUser() {
		User user = new User();
		when(userRepository.save(user)).thenReturn(user);
		userService.create(user);
		verify(userRepository).save(user);
	}

	@Test
	void testLogin() {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setEmail("test@email.com");
		loginRequest.setPassword("password");
		when(userRepository.existsByEmailAndPassword(anyString(), anyString())).thenReturn(true);
		userService.login(loginRequest);
		verify(userRepository).existsByEmailAndPassword("test@email.com", "password");
	}

	@Test
	void testLoginFail() {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setEmail("test@email.com");
		loginRequest.setPassword("password");
		when(userRepository.existsByEmailAndPassword(anyString(), anyString())).thenReturn(false);
		assertThrows(UserException.class, () -> userService.login(loginRequest));
	}

	@Test
	void testChangeLogin() {
		LoginChangeRequest loginChangeRequest = new LoginChangeRequest();
		User user = new User();
		user.setPassword("oldpass");
		loginChangeRequest.setEmail("test@email.com");
		loginChangeRequest.setOldPassword("oldpass");
		loginChangeRequest.setNewPassword("newpass");
		when(userRepository.findByEmailAndPassword(anyString(), anyString())).thenReturn(Optional.of(user));
		userService.changeLogin(loginChangeRequest);
		verify(userRepository).findByEmailAndPassword(anyString(), anyString());
	}

	@Test
	void testChangeLoginUserNotFoundFail() {
		LoginChangeRequest loginChangeRequest = new LoginChangeRequest();
		loginChangeRequest.setEmail("test@email.com");
		loginChangeRequest.setOldPassword("oldpass");
		loginChangeRequest.setNewPassword("newpass");
		when(userRepository.findByEmailAndPassword(anyString(), anyString()))
				.thenReturn(Optional.empty());
		assertThrows(UserException.class, () -> userService.changeLogin(loginChangeRequest));
		verify(userRepository).findByEmailAndPassword(anyString(), anyString());
	}

}
