package com.epam.mainservice.servicetest;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.mainservice.dao.Trainee;
import com.epam.mainservice.dao.Trainer;
import com.epam.mainservice.dao.Training;
import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.dao.User;
import com.epam.mainservice.exception.TrainingException;
import com.epam.mainservice.kafka.NotificationProducer;
import com.epam.mainservice.kafka.TrainerDetailsProducer;
import com.epam.mainservice.repo.TraineeRepository;
import com.epam.mainservice.repo.TrainerRepository;
import com.epam.mainservice.repo.TrainingRepository;
import com.epam.mainservice.repo.TrainingTypeRepository;
import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.mainservice.requestdata.TrainingCreationRequest;
import com.epam.mainservice.service.TrainerService;
import com.epam.mainservice.service.TrainingService;

@ExtendWith(MockitoExtension.class)
class TrainingServiceTest {

	@InjectMocks
	TrainingService trainingService;

	@Mock
	TrainingRepository trainingRepository;

	@Mock
	TraineeRepository traineeRepository;

	@Mock
	TrainerRepository trainerRepository;

	@Mock
	TrainingTypeRepository trainingTypeRepository;

	@Mock
	NotificationProducer notificationProducer;

	@Mock
	TrainerDetailsProducer trainerDetailsProducer;

	@Mock
	TrainerService trainerService;

	@Test
	void testCreateTraining() {
		User traineeUser = User.builder().firstName("trainee").lastName("trainee").email("trainee@email.com").build();
		Trainee trainee = Trainee.builder().user(traineeUser).trainingsList(new ArrayList<>())
				.trainersList(new ArrayList<>()).build();
		User trainerUser = User.builder().firstName("test").lastName("test").email("test@email.com").build();
		List<Trainee> traineeList = new ArrayList<>();
		Trainer trainer = Trainer.builder().user(trainerUser).traineeList(traineeList).trainingsList(new ArrayList<>())
				.trainingType(new TrainingType(0, "cardio")).build();
		Training training = new Training(0, trainee, trainer, "training", new TrainingType(0, "cardio"),
				LocalDate.now(), 1);
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainer));
		when(traineeRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainee));
		when(trainingRepository.save(any(Training.class))).thenReturn(training);
		doNothing().when(notificationProducer).sendNotification(any(NotificationDTO.class));
		TrainingCreationRequest trainingCreationRequest = new TrainingCreationRequest();
		trainingCreationRequest.setTraineeEmail("trainee@email.com");
		trainingCreationRequest.setTrainerEmail("test@email.com");
		trainingCreationRequest.setTrainingDate(LocalDate.now());
		trainingCreationRequest.setTrainingDuration(2);
		trainingCreationRequest.setTrainingName("training");
		trainingService.create(trainingCreationRequest);
		verify(trainerRepository).findByUserEmail(anyString());
		verify(traineeRepository).findByUserEmail(anyString());
		verify(trainingRepository).save(any(Training.class));
		verify(notificationProducer).sendNotification(any(NotificationDTO.class));
	}

	@Test
	void testCreateTrainingTrainerNotFoundFail() {
		TrainingCreationRequest trainingCreationRequest = new TrainingCreationRequest();
		trainingCreationRequest.setTraineeEmail("trainee@email.com");
		trainingCreationRequest.setTrainerEmail("test@email.com");
		trainingCreationRequest.setTrainingDate(LocalDate.now());
		trainingCreationRequest.setTrainingDuration(2);
		trainingCreationRequest.setTrainingName("training");
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.empty());
		assertThrows(TrainingException.class, () -> trainingService.create(trainingCreationRequest));
		verify(trainerRepository).findByUserEmail(anyString());
	}
	
	@Test
	void testCreateTrainingTraineeNotFoundFail() {
		User trainerUser = User.builder().firstName("test").lastName("test").email("test@email.com").build();
		List<Trainee> traineeList = new ArrayList<>();
		Trainer trainer = Trainer.builder().user(trainerUser).traineeList(traineeList).trainingsList(new ArrayList<>())
				.trainingType(new TrainingType(0, "cardio")).build();
		TrainingCreationRequest trainingCreationRequest = new TrainingCreationRequest();
		trainingCreationRequest.setTraineeEmail("trainee@email.com");
		trainingCreationRequest.setTrainerEmail("test@email.com");
		trainingCreationRequest.setTrainingDate(LocalDate.now());
		trainingCreationRequest.setTrainingDuration(2);
		trainingCreationRequest.setTrainingName("training");
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainer));
		when(traineeRepository.findByUserEmail(anyString())).thenReturn(Optional.empty());
		assertThrows(TrainingException.class, () -> trainingService.create(trainingCreationRequest));
		verify(trainerRepository).findByUserEmail(anyString());
		verify(traineeRepository).findByUserEmail(anyString());
	}
	
	@Test
	void testCreateTrainingTrainerAlreadyAssociatedWithTraineeFail() {
		User traineeUser = User.builder().firstName("trainee").lastName("trainee").email("trainee@email.com").build();
		Trainee trainee = Trainee.builder().user(traineeUser).trainingsList(new ArrayList<>())
				.trainersList(new ArrayList<>()).build();
		User trainerUser = User.builder().firstName("test").lastName("test").email("test@email.com").build();
		List<Trainee> traineeList = List.of(trainee);
		Trainer trainer = Trainer.builder().user(trainerUser).traineeList(traineeList).trainingsList(new ArrayList<>())
				.trainingType(new TrainingType(0, "cardio")).build();
		TrainingCreationRequest trainingCreationRequest = new TrainingCreationRequest();
		trainingCreationRequest.setTraineeEmail("trainee@email.com");
		trainingCreationRequest.setTrainerEmail("test@email.com");
		trainingCreationRequest.setTrainingDate(LocalDate.now());
		trainingCreationRequest.setTrainingDuration(2);
		trainingCreationRequest.setTrainingName("training");
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainer));
		when(traineeRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainee));
		assertThrows(TrainingException.class, () -> trainingService.create(trainingCreationRequest));
		verify(trainerRepository).findByUserEmail(anyString());
		verify(traineeRepository).findByUserEmail(anyString());
	}
}
