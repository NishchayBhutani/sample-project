package com.epam.mainservice.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.mainservice.dao.Trainee;
import com.epam.mainservice.dao.Trainer;
import com.epam.mainservice.dao.Training;
import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.dao.User;
import com.epam.mainservice.exception.TrainerException;
import com.epam.mainservice.kafka.NotificationProducer;
import com.epam.mainservice.repo.TrainerRepository;
import com.epam.mainservice.repo.TrainingTypeRepository;
import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.mainservice.requestdata.TrainerCreationRequest;
import com.epam.mainservice.requestdata.TrainerTrainingsListRequest;
import com.epam.mainservice.requestdata.TrainerUpdateRequest;
import com.epam.mainservice.responsedata.TraineeInfo;
import com.epam.mainservice.responsedata.TrainerProfileResponse;
import com.epam.mainservice.responsedata.TrainerTrainingsListResponse;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.service.TrainerService;
import com.epam.mainservice.service.UserService;

@ExtendWith(MockitoExtension.class)
class TrainerServiceTest {
	@InjectMocks
	TrainerService trainerService;

	@Mock
	TrainerRepository trainerRepository;

	@Mock
	UserService userService;

	@Mock
	TrainingTypeRepository trainingTypeRepository;

	@Mock
	NotificationProducer notificationProducer;

	@Mock
	PasswordEncoder passwordEncoder;

	@Test
	void testRegisterTrainer() {
		User user = User.builder().email("test@email.com").firstName("test").lastName("test").isActive(true)
				.password("pass").build();
		when(userService.create(any(User.class))).thenReturn(user);
		TrainingType trainingType = new TrainingType(0, "cardio");
		when(trainingTypeRepository.findByTrainingTypeName(anyString())).thenReturn(Optional.of(trainingType));
		when(trainingTypeRepository.save(any(TrainingType.class))).thenReturn(trainingType);
		Trainer trainer = Trainer.builder().user(user).build();
		when(trainerRepository.save(any(Trainer.class))).thenReturn(trainer);
		UserCreationResponse userCreationResponse = new UserCreationResponse("test@email.com", "randomPass");
		TrainerCreationRequest trainerCreationRequest = new TrainerCreationRequest();
		trainerCreationRequest.setEmail("test@email.com");
		trainerCreationRequest.setFirstName("test");
		trainerCreationRequest.setLastName("test");
		trainerCreationRequest.setTrainingTypeName("cardio");
		UserCreationResponse result = trainerService.register(trainerCreationRequest);
		assertEquals(userCreationResponse.getEmail(), result.getEmail());
		verify(userService).create(any(User.class));
		verify(trainingTypeRepository).findByTrainingTypeName(anyString());
		verify(trainingTypeRepository).save(any(TrainingType.class));
		verify(trainerRepository).save(any(Trainer.class));
	}

	@Test
	void testRegisterTrainerTrainingTypeNotFoundFail() {
		User user = User.builder().email("test@email.com").firstName("test").lastName("test").isActive(true)
				.password("pass").build();
		when(userService.create(any(User.class))).thenReturn(user);
		when(trainingTypeRepository.findByTrainingTypeName(anyString())).thenReturn(Optional.empty());
		TrainerCreationRequest trainerCreationRequest = new TrainerCreationRequest();
		trainerCreationRequest.setEmail("test@email.com");
		trainerCreationRequest.setFirstName("test");
		trainerCreationRequest.setLastName("test");
		trainerCreationRequest.setTrainingTypeName("cardio");
		assertThrows(TrainerException.class, () -> trainerService.register(trainerCreationRequest));
		verify(userService).create(any(User.class));
		verify(trainingTypeRepository).findByTrainingTypeName(anyString());
	}
	
	@Test
	void testGetTrainerProfile() {
		User user = User.builder().email("test@email.com").firstName("test").lastName("test").isActive(true)
				.password("pass").build();
		User traineeUser = User.builder().firstName("test").lastName("test").email("trainee@email.com").build();
		Trainee trainee = new Trainee();
		trainee.setUser(traineeUser);
		Trainer trainer = Trainer.builder().user(user).trainingType(new TrainingType(0, "cardio"))
				.traineeList(List.of(trainee)).build();
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainer));
		List<TraineeInfo> traineeInfoList = List.of(new TraineeInfo("trainee@email.com", "test", "test"));
		TrainerProfileResponse trainerProfileResponse = new TrainerProfileResponse("test@email.com", "test", "test",
				new TrainingType(0, "cardio"), true, traineeInfoList);
		TrainerProfileResponse result = trainerService.getProfile("test@email.com");
		assertEquals(result, trainerProfileResponse);
		verify(trainerRepository).findByUserEmail(anyString());
	}
	
	@Test
	void testGetTrainerProfileTrainerNotFoundFail() {
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.empty());
		assertThrows(TrainerException.class, () -> trainerService.getProfile("email@email.com"));
		verify(trainerRepository).findByUserEmail(anyString());
	}

	@Test
	void testUpdateTrainerProfile() {
		User user = User.builder().email("test@email.com").firstName("test").lastName("test").isActive(true)
				.password("pass").build();
		User traineeUser = User.builder().firstName("test").lastName("test").email("trainee@email.com").build();
		Trainee trainee = new Trainee();
		trainee.setUser(traineeUser);
		Trainer trainer = Trainer.builder().user(user).traineeList(List.of(trainee))
				.trainingType(new TrainingType(0, "cardio")).build();
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainer));
		User updatedUser = user;
		updatedUser.setFirstName("updatetest");
		Trainer updatedTrainer = trainer;
		updatedTrainer.setUser(updatedUser);
		when(trainerRepository.save(any(Trainer.class))).thenReturn(trainer);
		List<TraineeInfo> traineeInfoList = List.of(new TraineeInfo("trainee@email.com", "test", "test"));
		TrainerProfileResponse trainerProfileResponse = new TrainerProfileResponse("test@email.com", "updatetest",
				"test", new TrainingType(0, "cardio"), false, traineeInfoList);
		doNothing().when(notificationProducer).sendNotification(any(NotificationDTO.class));
		TrainerUpdateRequest trainerUpdateRequest = new TrainerUpdateRequest();
		trainerUpdateRequest.setFirstName("updatetest");
		trainerUpdateRequest.setLastName("test");
		TrainerProfileResponse result = trainerService.updateProfile("test@email.com", trainerUpdateRequest);
		assertEquals(result, trainerProfileResponse);
	}
	
	@Test
	void testUpdateTrainerProfileTrainerNotFoundFail() {
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.empty());
		TrainerUpdateRequest trainerUpdateRequest = new TrainerUpdateRequest();
		trainerUpdateRequest.setFirstName("updatetest");
		trainerUpdateRequest.setLastName("test");
		assertThrows(TrainerException.class, () -> trainerService.updateProfile("email@email.com", trainerUpdateRequest));
		verify(trainerRepository).findByUserEmail(anyString());
	}

	@Test
	void testGetTrainingsList() {
		Trainer trainer = new Trainer();
		TrainingType trainingType = new TrainingType();
		User user = new User();
		user.setFirstName("Alice");
		trainingType.setTrainingTypeName("Type A");
		trainer.setUser(user);
		Training training = new Training();
		training.setTrainingName("Training 1");
		training.setTrainingDate(LocalDate.of(2022, 6, 1));
		training.setTrainingType(trainingType);
		training.setTrainingDuration(3);
		training.setTrainer(trainer);
		List<Training> trainingList = new ArrayList<>();
		trainingList.add(training);
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(java.util.Optional.of(trainer));
		when(trainerRepository.findAllTrainingInBetween(any(), any(), any())).thenReturn(trainingList);
		TrainerTrainingsListRequest request = new TrainerTrainingsListRequest();
		request.setFrom(LocalDate.of(2022, 1, 1));
		request.setTo(LocalDate.of(2022, 12, 31));
		request.setTraineeName("Alice");
		List<TrainerTrainingsListResponse> result = trainerService.getTrainingsList("trainer@example.com", request);
		assertNotNull(result);
		assertEquals(1, result.size());
		TrainerTrainingsListResponse response = result.get(0);
		assertEquals("Training 1", response.getTrainingName());
		assertEquals(LocalDate.of(2022, 6, 1), response.getTrainingDate());
		assertEquals("Type A", response.getTrainingType().getTrainingTypeName());
		assertEquals(3, response.getTrainingDuration());
		assertEquals("Alice", response.getTraineeName());
	}

	@Test
	void testGetTrainingsListWithNoTrainings() {
		TrainerTrainingsListRequest request = new TrainerTrainingsListRequest();
		request.setFrom(LocalDate.of(2022, 1, 1));
		request.setTo(LocalDate.of(2022, 12, 31));
		request.setTraineeName("Alice");
		Trainer trainer = new Trainer();
		trainer.setId(1);
		List<Training> trainingList = new ArrayList<>();
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(java.util.Optional.of(trainer));
		when(trainerRepository.findAllTrainingInBetween(any(), any(), any())).thenReturn(trainingList);
		List<TrainerTrainingsListResponse> result = trainerService.getTrainingsList("trainer@example.com", request);
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test
	void testGetTrainingsListTrainerNotFoundFail() {
		when(trainerRepository.findByUserEmail(anyString())).thenReturn(Optional.empty());
		TrainerTrainingsListRequest request = new TrainerTrainingsListRequest();
		request.setFrom(LocalDate.of(2022, 1, 1));
		request.setTo(LocalDate.of(2022, 12, 31));
		request.setTraineeName("Alice");
		assertThrows(TrainerException.class, () -> trainerService.getTrainingsList("email@email.com", request));
		verify(trainerRepository).findByUserEmail(anyString());
	}

}
