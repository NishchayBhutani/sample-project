package com.epam.mainservice.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.mainservice.dao.Trainee;
import com.epam.mainservice.dao.Trainer;
import com.epam.mainservice.dao.Training;
import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.dao.User;
import com.epam.mainservice.exception.TraineeException;
import com.epam.mainservice.exception.UserException;
import com.epam.mainservice.kafka.NotificationProducer;
import com.epam.mainservice.repo.TraineeRepository;
import com.epam.mainservice.repo.TrainerRepository;
import com.epam.mainservice.repo.TrainingTypeRepository;
import com.epam.mainservice.requestdata.NotificationDTO;
import com.epam.mainservice.requestdata.TraineeCreationRequest;
import com.epam.mainservice.requestdata.TraineeTrainingsListRequest;
import com.epam.mainservice.requestdata.TraineeUpdateRequest;
import com.epam.mainservice.requestdata.TrainerEmailListRequest;
import com.epam.mainservice.responsedata.TraineeProfileResponse;
import com.epam.mainservice.responsedata.TraineeTrainingsListResponse;
import com.epam.mainservice.responsedata.TrainerInfo;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.service.TraineeService;
import com.epam.mainservice.service.UserService;

@ExtendWith(MockitoExtension.class)
class TraineeServiceTest {

	@InjectMocks
	TraineeService traineeService;

	@Mock
	TraineeRepository traineeRepository;

	@Mock
	TrainerRepository trainerRepository;

	@Mock
	TrainingTypeRepository trainingTypeRepository;

	@Mock
	UserService userService;

	@Mock
	NotificationProducer notificationProducer;

	@Mock
	PasswordEncoder passwordEncoder;

	User user;
	Trainee trainee;

	@BeforeEach
	void setUp() {
		user = User.builder().firstName("test").lastName("test").email("test@email.com").isActive(true).build();
		trainee = Trainee.builder().address("address").dob(LocalDate.now()).user(user)
				.trainersList(List.of(new Trainer(0, user, new TrainingType(0, "cardio"), new ArrayList<Trainee>(),
						new ArrayList<Training>())))
				.build();
	}

	@Test
	void testCreateTrainee() {
		TraineeCreationRequest traineeCreationRequest = new TraineeCreationRequest();
		traineeCreationRequest.setFirstName("test");
		traineeCreationRequest.setLastName("test");
		traineeCreationRequest.setEmail("test@email.com");
		traineeCreationRequest.setDob(LocalDate.now());
		traineeCreationRequest.setAddress("address");
		when(userService.create(user)).thenReturn(user);
		trainee = Trainee.builder().address("address").dob(LocalDate.now()).user(user).build();
		when(traineeRepository.save(trainee)).thenReturn(trainee);
		doNothing().when(notificationProducer).sendNotification(any(NotificationDTO.class));
		UserCreationResponse result = traineeService.create(traineeCreationRequest);
		assertEquals("test@email.com", result.getEmail());
		verify(userService).create(user);
		verify(traineeRepository).save(trainee);
		verify(notificationProducer).sendNotification(any(NotificationDTO.class));
	}

	@Test
	void testGetTraineeProfile() {
		List<TrainerInfo> trainersInfoList = List
				.of(new TrainerInfo("test@email.com", "test", "test", new TrainingType(0, "cardio")));
		when(traineeRepository.findByUserEmail("test@email.com")).thenReturn(Optional.of(trainee));
		TraineeProfileResponse traineeProfileResponse = new TraineeProfileResponse("test@email.com", "test", "test",
				LocalDate.now(), "address", true, trainersInfoList);
		TraineeProfileResponse result = traineeService.getProfile("test@email.com");
		assertEquals(traineeProfileResponse, result);
		verify(traineeRepository).findByUserEmail("test@email.com");
	}

	@Test
	void testGetTraineeProfileTrineeNotFoundFail() {
		when(traineeRepository.findByUserEmail(anyString())).thenThrow(new UserException("trainee not found"));
		assertThrows(UserException.class, () -> traineeService.getProfile("test@email.com"));
		verify(traineeRepository).findByUserEmail("test@email.com");
	}

	@Test
	void testUpdateTraineeProfile() {
		when(traineeRepository.findByUserEmail("test@email.com")).thenReturn(Optional.of(trainee));
		TraineeUpdateRequest traineeUpdateRequest = new TraineeUpdateRequest();
		traineeUpdateRequest.setFirstName("updatedtest");
		traineeUpdateRequest.setLastName("updatedtest");
		traineeUpdateRequest.setDob(LocalDate.now());
		traineeUpdateRequest.setActive(false);
		traineeUpdateRequest.setAddress("updatedaddress");
		User updatedUser = user;
		updatedUser.setFirstName("updatedtest");
		updatedUser.setLastName("updatedtest");
		updatedUser.setActive(false);
		Trainee updatedTrainee = trainee;
		updatedTrainee.setAddress("updatedaddress");
		updatedTrainee.setDob(LocalDate.now());
		List<TrainerInfo> trainersInfoList = List
				.of(new TrainerInfo("test@email.com", "updatedtest", "updatedtest", new TrainingType(0, "cardio")));
		TraineeProfileResponse traineeProfileResponse = new TraineeProfileResponse("test@email.com", "updatedtest", "updatedtest",
				LocalDate.now(), "updatedaddress", false, trainersInfoList);
		TraineeProfileResponse result = traineeService.updateProfile("test@email.com", traineeUpdateRequest);
		assertEquals(traineeProfileResponse, result);
		verify(traineeRepository).findByUserEmail(anyString());
	}

	@Test
	void testUpdateTraineeProfileTraineeNotFoundFail() {
		when(traineeRepository.findByUserEmail(anyString())).thenThrow(new TraineeException("trainee not found"));
		assertThrows(TraineeException.class, () -> traineeService.updateProfile("test@email.com", new TraineeUpdateRequest()));
		verify(traineeRepository).findByUserEmail("test@email.com");
	}

	@Test
	void testUpdateTraineeTrainersList() {
		Trainer trainer = Trainer.builder().user(user).trainingType(new TrainingType(0, "cardio")).build();
		trainee.setUser(User.builder().email("email@email.com").build());
		TrainerEmailListRequest trainerEmailListRequest = new TrainerEmailListRequest();
		trainerEmailListRequest.setTrainerEmailList(List.of("email@email.com"));
		when(traineeRepository.findByUserEmail("test@email.com")).thenReturn(Optional.of(trainee));
		when(trainerRepository.findByUserEmail("email@email.com")).thenReturn(Optional.of(trainer));
		List<TrainerInfo> trainersInfoList = List
				.of(new TrainerInfo("test@email.com", "test", "test", new TrainingType(0, "cardio")));
		TraineeProfileResponse traineeProfileResponse = new TraineeProfileResponse("test@email.com", null, null,
				LocalDate.now(), "address", false, trainersInfoList);
		TraineeProfileResponse result = traineeService.updateTrainersList("test@email.com", trainerEmailListRequest);
		assertEquals(traineeProfileResponse, result);
		verify(traineeRepository).findByUserEmail("test@email.com");
		verify(trainerRepository).findByUserEmail("email@email.com");
	}

	@Test
	void testUpdateTraineeTrainersListTraineeNotFoundFail() {
		when(traineeRepository.findByUserEmail(anyString())).thenThrow(new TraineeException("trainee not found"));
		assertThrows(TraineeException.class, () -> traineeService.updateTrainersList("test@email.com", new TrainerEmailListRequest()));
		verify(traineeRepository).findByUserEmail("test@email.com");
	}

	@Test
	void testDeleteTraineeProfile() {
		doNothing().when(traineeRepository).deleteByUserEmail("test@email.com");
		traineeService.deleteProfile("test@email.com");
		verify(traineeRepository).deleteByUserEmail("test@email.com");
	}

	@Test
	void testGetTrainingsList() {
		LocalDate fromDate = LocalDate.of(2022, 1, 1);
		LocalDate toDate = LocalDate.of(2022, 12, 31);
		Trainee trainee = new Trainee();
		Training training = new Training();
		training.setTrainingName("Java Basics");
		training.setTrainingDate(LocalDate.of(2022, 3, 15));
		TrainingType trainingType = new TrainingType();
		trainingType.setTrainingTypeName("Technical");
		training.setTrainingType(trainingType);
		training.setTrainingDuration(3);
		Trainer trainer = new Trainer();
		User trainerUser = new User();
		trainerUser.setFirstName("John");
		trainer.setUser(trainerUser);
		training.setTrainer(trainer);
		TraineeTrainingsListRequest request = new TraineeTrainingsListRequest();
		request.setFrom(fromDate);
		request.setTo(toDate);
		request.setTrainerName("John");
		request.setTrainingType("Technical");
		List<Training> filteredTrainingsList = Collections.singletonList(training);
		when(traineeRepository.findByUserEmail(any())).thenReturn(Optional.of(trainee));
		when(traineeRepository.findAllTrainingInBetween(any(), any(), any())).thenReturn(filteredTrainingsList);
		List<TraineeTrainingsListResponse> result = traineeService.getTrainingsList("trainee@example.com", request);
		verify(traineeRepository).findByUserEmail(eq("trainee@example.com"));
		verify(traineeRepository).findAllTrainingInBetween(eq(fromDate), eq(toDate), any());
		assertEquals(1, result.size());
		TraineeTrainingsListResponse response = result.get(0);
		assertEquals("Java Basics", response.getName());
		assertEquals(LocalDate.of(2022, 3, 15), response.getDate());
		assertEquals("Technical", response.getTrainingType().getTrainingTypeName());
		assertEquals(3, response.getTrainingDuration());
		assertEquals("John", response.getTrainerName());
	}

	@Test
	    void testGetTrainingsListTraineeNotFoundFail() {
	        when(traineeRepository.findByUserEmail(any())).thenReturn(Optional.empty());
	        assertThrows(TraineeException.class,
	                () -> traineeService.getTrainingsList("nonexistent@example.com", new TraineeTrainingsListRequest()));
	    }

	@Test
	void testGetTrainingsListEmptyTrainings() {
		Trainee trainee = new Trainee();
		trainee.setUser(User.builder().email("trainee@example.com").build());
		TraineeTrainingsListRequest request = new TraineeTrainingsListRequest();
		when(traineeRepository.findByUserEmail(any())).thenReturn(Optional.of(trainee));
		when(traineeRepository.findAllTrainingInBetween(any(), any(), any())).thenReturn(new ArrayList<>());
		List<TraineeTrainingsListResponse> result = traineeService.getTrainingsList("trainee@example.com", request);
		verify(traineeRepository).findByUserEmail(eq("trainee@example.com"));
		verify(traineeRepository).findAllTrainingInBetween(any(), any(), any());
		assertTrue(result.isEmpty());
	}

	@Test
	void testGetUnassociatedTrainersList() {
		Trainer trainer = Trainer.builder().user(user).trainingType(new TrainingType(0, "cardio")).build();
		when(traineeRepository.findByUserEmail(anyString())).thenReturn(Optional.of(trainee));
		when(trainerRepository.findAll()).thenReturn(List.of(trainer));
		List<TrainerInfo> trainerInfoList = List
				.of(new TrainerInfo("test@email.com", "test", "test", new TrainingType(0, "cardio")));
		List<TrainerInfo> result = traineeService.getUnassociatedTrainersList("test@test.com");
		assertEquals(trainerInfoList, result);

	}

	@Test
	void testGetUnassociatedTrainersListTraineeNotFoundFail() {
		when(traineeRepository.findByUserEmail(anyString())).thenThrow(new TraineeException("trainee not found"));
		assertThrows(TraineeException.class, () -> traineeService.getUnassociatedTrainersList("test@email.com"));
		verify(traineeRepository).findByUserEmail("test@email.com");
	}
	
	
}
