package com.epam.mainservice.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.repo.TrainingTypeRepository;
import com.epam.mainservice.service.TrainingTypeService;

@ExtendWith(MockitoExtension.class)
public class TrainingTypeServiceTest {

	@InjectMocks
	TrainingTypeService trainingTypeService;

	@Mock
	TrainingTypeRepository trainingTypeRepository;

	@Test
	void getAllTrainingTypes() {
		List<TrainingType> trainingTypesList = List.of(new TrainingType(0, "cardio"));
		when(trainingTypeRepository.findAll()).thenReturn(trainingTypesList);
		List<TrainingType> result = trainingTypeService.getAll();
		assertEquals(trainingTypesList, result);
		verify(trainingTypeRepository).findAll();
	}

}
