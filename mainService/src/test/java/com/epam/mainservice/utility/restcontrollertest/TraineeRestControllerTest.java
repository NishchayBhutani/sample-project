package com.epam.mainservice.utility.restcontrollertest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.requestdata.TraineeCreationRequest;
import com.epam.mainservice.requestdata.TraineeTrainingsListRequest;
import com.epam.mainservice.requestdata.TraineeUpdateRequest;
import com.epam.mainservice.requestdata.TrainerEmailListRequest;
import com.epam.mainservice.responsedata.TraineeProfileResponse;
import com.epam.mainservice.responsedata.TraineeTrainingsListResponse;
import com.epam.mainservice.responsedata.TrainerInfo;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.restcontroller.TraineeRestController;
import com.epam.mainservice.service.TraineeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@WebMvcTest(TraineeRestController.class)
class TraineeRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TraineeService traineeService;

	static ObjectMapper mapper;

	@BeforeAll
	static void setUp() {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}

	@Test
	void testCreateTrainee() throws Exception {
		TraineeCreationRequest request = new TraineeCreationRequest();
		request.setFirstName("John");
		request.setLastName("Doe");
		request.setEmail("john.doe@example.com");
		request.setDob(LocalDate.of(1990, 5, 15));
		request.setAddress("123 Main St");
		UserCreationResponse response = new UserCreationResponse("john.doe@example.com", "password");
		when(traineeService.create(any(TraineeCreationRequest.class))).thenReturn(response);
		mockMvc.perform(
				post("/gymapp/trainees").contentType("application/json").content(mapper.writeValueAsString(request)))
				.andExpect(status().isCreated()).andExpect(jsonPath("$.email").value("john.doe@example.com"));
	}

	@Test
	void testGetTraineeProfile() throws Exception {
		List<TrainerInfo> trainersList = new ArrayList<>();
		TraineeProfileResponse response = new TraineeProfileResponse("john.doe@example.com", "John", "Doe",
				LocalDate.of(1990, 5, 15), "123 Main St", true, trainersList);
		when(traineeService.getProfile(anyString())).thenReturn(response);
		mockMvc.perform(get("/gymapp/trainees/{email}", "john.doe@example.com")).andExpect(status().isOk())
				.andExpect(jsonPath("$.email").value("john.doe@example.com"))
				.andExpect(jsonPath("$.firstName").value("John")).andExpect(jsonPath("$.lastName").value("Doe"))
				.andExpect(jsonPath("$.dob").value("1990-05-15")).andExpect(jsonPath("$.address").value("123 Main St"));
	}

	@Test
	void testUpdateTraineeProfile() throws Exception {
		TraineeUpdateRequest request = new TraineeUpdateRequest();
		request.setFirstName("Updated John");
		request.setLastName("Updated Doe");
		request.setDob(LocalDate.of(1995, 8, 20));
		request.setAddress("456 Elm St");
		request.setActive(false);
		List<TrainerInfo> trainersList = new ArrayList<>();
		TraineeProfileResponse response = new TraineeProfileResponse("john.doe@example.com", "Updated John",
				"Updated Doe", LocalDate.of(1995, 8, 20), "456 Elm St", false, trainersList);
		when(traineeService.updateProfile(anyString(), any(TraineeUpdateRequest.class))).thenReturn(response);
		mockMvc.perform(put("/gymapp/trainees/{email}", "john.doe@example.com").contentType("application/json")
				.content(mapper.writeValueAsString(request))).andExpect(status().isOk())
				.andExpect(jsonPath("$.email").value("john.doe@example.com"))
				.andExpect(jsonPath("$.firstName").value("Updated John"))
				.andExpect(jsonPath("$.lastName").value("Updated Doe")).andExpect(jsonPath("$.dob").value("1995-08-20"))
				.andExpect(jsonPath("$.address").value("456 Elm St"));
	}

	@Test
	void testDeleteTraineeProfile() throws Exception {
		mockMvc.perform(delete("/gymapp/trainees/{email}", "john.doe@example.com")).andExpect(status().isOk());
	}

	@Test
	void testGetUnassociatedTraineeTrainers() throws Exception {
		String email = "john@example.com";
		List<TrainerInfo> trainerInfoList = new ArrayList<>();
		TrainerInfo trainer1 = new TrainerInfo("trainer1@example.com", "Trainer", "One",
				new TrainingType(0, "aerobics"));
		trainerInfoList.add(trainer1);
		TrainerInfo trainer2 = new TrainerInfo("trainer2@example.com", "Trainer", "Two", new TrainingType(1, "yoga"));
		trainerInfoList.add(trainer2);
		when(traineeService.getUnassociatedTrainersList(email)).thenReturn(trainerInfoList);
		mockMvc.perform(get("/gymapp/trainees/{email}/trainers", email)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].email").value(trainerInfoList.get(0).getEmail()))
				.andExpect(jsonPath("$[0].firstName").value(trainerInfoList.get(0).getFirstName()))
				.andExpect(jsonPath("$[0].lastName").value(trainerInfoList.get(0).getLastName()))
				.andExpect(jsonPath("$[0].trainingType.trainingTypeName")
						.value(trainerInfoList.get(0).getTrainingType().getTrainingTypeName()))
				.andExpect(jsonPath("$[1].email").value(trainerInfoList.get(1).getEmail()))
				.andExpect(jsonPath("$[1].firstName").value(trainerInfoList.get(1).getFirstName()))
				.andExpect(jsonPath("$[1].lastName").value(trainerInfoList.get(1).getLastName()))
				.andExpect(jsonPath("$[1].trainingType.trainingTypeName")
						.value(trainerInfoList.get(1).getTrainingType().getTrainingTypeName()));
	}

	@Test
	void testUpdateTraineeTrainers() throws Exception {
		String email = "test@example.com";
		TrainerEmailListRequest trainerEmailListRequest = new TrainerEmailListRequest();
		List<String> emailList = Arrays.asList("trainer1@example.com", "trainer2@example.com");
		trainerEmailListRequest.setTrainerEmailList(emailList);
		TraineeProfileResponse expectedResponse = new TraineeProfileResponse();
		when(traineeService.updateTrainersList(email, trainerEmailListRequest)).thenReturn(expectedResponse);
		mockMvc.perform(put("/gymapp/trainees/{email}/trainers", email).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(trainerEmailListRequest))).andExpect(status().isOk())
				.andExpect(jsonPath("$.email").value(expectedResponse.getEmail()))
				.andExpect(jsonPath("$.firstName").value(expectedResponse.getFirstName()))
				.andExpect(jsonPath("$.lastName").value(expectedResponse.getLastName())).andReturn();
	}

	@Test
	void testGetTraineeTrainings() throws Exception {
		String email = "test@example.com";
		LocalDate from = LocalDate.of(2023, 1, 1);
		LocalDate to = LocalDate.of(2023, 12, 31);
		String trainerName = "Trainer Name";
		String trainingType = "Some Training Type";
		TraineeTrainingsListRequest request = new TraineeTrainingsListRequest(from, to, trainerName, trainingType);
		String name = "Training Name";
		LocalDate date = LocalDate.now();
		TrainingType trainingTypeEnum = new TrainingType(0, "aerobics");
		int trainingDuration = 5;
		String responseTrainerName = "Trainer Name";
		TraineeTrainingsListResponse expectedResponse = new TraineeTrainingsListResponse(name, date, trainingTypeEnum,
				trainingDuration, responseTrainerName);
		when(traineeService.getTrainingsList(email, request)).thenReturn(Arrays.asList(expectedResponse));
		mockMvc.perform(get("/gymapp/trainees/{email}/trainings", email).contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request))).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].name").value(expectedResponse.getName()))
				.andExpect(jsonPath("$[0].date").value(expectedResponse.getDate().toString()))
				.andExpect(jsonPath("$[0].trainingType.trainingTypeName")
						.value(expectedResponse.getTrainingType().getTrainingTypeName()))
				.andExpect(jsonPath("$[0].trainingDuration").value(expectedResponse.getTrainingDuration()))
				.andExpect(jsonPath("$[0].trainerName").value(expectedResponse.getTrainerName())).andReturn();
	}

}
