package com.epam.mainservice.utility.restcontrollertest;

import static org.mockito.Mockito.verify;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.mainservice.requestdata.TrainingCreationRequest;
import com.epam.mainservice.restcontroller.TrainingRestController;
import com.epam.mainservice.service.TrainingService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@WebMvcTest(TrainingRestController.class)
class TrainingRestControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	TrainingService trainingService;

	
	static ObjectMapper mapper;

	@BeforeAll
	static void setUp() {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}
	
	@Test
	void testCreateTraining() throws Exception {
		TrainingCreationRequest request = new TrainingCreationRequest();
		request.setTraineeEmail("trainee@example.com");
		request.setTrainerEmail("trainer@example.com");
		request.setTrainingName("Training A");
		request.setTrainingDate(LocalDate.now());
		request.setTrainingDuration(60);

		mockMvc.perform(MockMvcRequestBuilders.post("/gymapp/trainings").contentType("application/json")
				.content(mapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		verify(trainingService).create(request);
	}
}
