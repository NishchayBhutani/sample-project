package com.epam.mainservice.utility.restcontrollertest;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.epam.mainservice.requestdata.LoginChangeRequest;
import com.epam.mainservice.requestdata.LoginRequest;
import com.epam.mainservice.restcontroller.UserRestController;
import com.epam.mainservice.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@WebMvcTest(UserRestController.class)
class UserRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService;

	static ObjectMapper mapper;

	@BeforeAll
	static void setUp() {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}

	@Test
	void testLoginUser() throws Exception {
		LoginRequest request = new LoginRequest();
		request.setEmail("user@example.com");
		request.setPassword("password");

		mockMvc.perform(MockMvcRequestBuilders.post("/gymapp/users").contentType("application/json")
				.content(mapper.writeValueAsString(request))).andExpect(MockMvcResultMatchers.status().isOk());

		verify(userService).login(request);
	}

	@Test
	void testChangeUserLogin() throws Exception {
		LoginChangeRequest request = new LoginChangeRequest();
		request.setEmail("user@example.com");
		request.setOldPassword("oldpassword");
		request.setNewPassword("newpassword");

		mockMvc.perform(MockMvcRequestBuilders.put("/gymapp/users").contentType("application/json")
				.content(mapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		verify(userService).changeLogin(request);
	}
}
