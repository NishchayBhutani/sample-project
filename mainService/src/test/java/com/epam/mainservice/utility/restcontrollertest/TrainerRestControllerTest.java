package com.epam.mainservice.utility.restcontrollertest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.mainservice.dao.TrainingType;
import com.epam.mainservice.requestdata.TrainerCreationRequest;
import com.epam.mainservice.requestdata.TrainerUpdateRequest;
import com.epam.mainservice.responsedata.TrainerProfileResponse;
import com.epam.mainservice.responsedata.TrainerTrainingsListResponse;
import com.epam.mainservice.responsedata.UserCreationResponse;
import com.epam.mainservice.restcontroller.TrainerRestController;
import com.epam.mainservice.service.TrainerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@WebMvcTest(TrainerRestController.class)
class TrainerRestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TrainerService trainerService;

	static ObjectMapper mapper;

	@BeforeAll
	static void setUp() {
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
	}

	@Test
	void testCreateTrainer() throws Exception {
		TrainerCreationRequest request = new TrainerCreationRequest();
		request.setFirstName("John");
		request.setLastName("Doe");
		request.setEmail("john@example.com");
		request.setTrainingTypeName("cardio");
		UserCreationResponse response = new UserCreationResponse("john@example.com", "password");
		when(trainerService.register(request)).thenReturn(response);
		mockMvc.perform(post("/gymapp/trainers").contentType("application/json").content(mapper.writeValueAsString(request)))
				.andExpect(status().isCreated()).andExpect(jsonPath("$.email").value("john@example.com"))
				.andExpect(jsonPath("$.password").value("password"));
	}

	@Test
	void testGetTrainerProfile() throws Exception {
		TrainerProfileResponse response = new TrainerProfileResponse("john@example.com", "John", "Doe",
				new TrainingType(0, "cardio"), true, List.of());
		when(trainerService.getProfile("john@example.com")).thenReturn(response);
		mockMvc.perform(get("/gymapp/trainers/john@example.com")).andExpect(status().isOk())
				.andExpect(jsonPath("$.email").value("john@example.com"))
				.andExpect(jsonPath("$.firstName").value("John")).andExpect(jsonPath("$.lastName").value("Doe"));
	}

	@Test
	void testUpdateTrainerProfile() throws Exception {
		TrainerUpdateRequest request = new TrainerUpdateRequest();
		request.setFirstName("Jane");
		request.setLastName("Smith");
		request.setActive(true);
		TrainerProfileResponse response = new TrainerProfileResponse("john@example.com", "Jane", "Smith", null, true,
				null);
		when(trainerService.updateProfile("john@example.com", request)).thenReturn(response);
		mockMvc.perform(put("/gymapp/trainers/john@example.com").contentType("application/json")
				.content(mapper.writeValueAsString(request))).andExpect(status().isOk())
				.andExpect(jsonPath("$.email").value("john@example.com"))
				.andExpect(jsonPath("$.firstName").value("Jane")).andExpect(jsonPath("$.lastName").value("Smith"));
	}

	@Test
	void testGetTrainerTrainings() throws Exception {
		List<TrainerTrainingsListResponse> responseList = new ArrayList<>();
		responseList.add(new TrainerTrainingsListResponse("Training A", LocalDate.now(), null, 60, "Trainee A"));
		responseList.add(new TrainerTrainingsListResponse("Training B", LocalDate.now(), null, 90, "Trainee B"));
		when(trainerService.getTrainingsList("john@example.com", null)).thenReturn(responseList);
		mockMvc.perform(get("/gymapp/trainers/john@example.com/trainings")).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].trainingName").value("Training A"))
				.andExpect(jsonPath("$[1].trainingName").value("Training B"));
	}
}
