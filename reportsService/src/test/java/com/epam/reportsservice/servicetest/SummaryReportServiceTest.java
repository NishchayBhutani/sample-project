package com.epam.reportsservice.servicetest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.mainservice.responsedata.TrainerDetailsResponse;
import com.epam.reportsservice.dao.SummaryReport;
import com.epam.reportsservice.dto.SummaryReportDTO;
import com.epam.reportsservice.dto.TrainerTrainingsListResponse;
import com.epam.reportsservice.dto.TrainingType;
import com.epam.reportsservice.repo.SummaryReportRepository;
import com.epam.reportsservice.service.SummaryReportService;

@ExtendWith(MockitoExtension.class)
class SummaryReportServiceTest {

	@InjectMocks
	SummaryReportService summaryReportService;

	@Mock
	SummaryReportRepository summaryReportRepository;

	@Mock
	ModelMapper modelMapper;

	@Test
	void testCreateReport() {
		TrainerTrainingsListResponse trainerTrainingsListResponse1 = new TrainerTrainingsListResponse("New Training",
				LocalDate.of(2023, 1, 1), new TrainingType(1, "cardio"), 2, "trainee");
		TrainerTrainingsListResponse trainerTrainingsListResponse2 = new TrainerTrainingsListResponse("New Training",
				LocalDate.of(2023, 2, 1), new TrainingType(1, "cardio"), 2, "trainee");
		TrainerTrainingsListResponse trainerTrainingsListResponse3 = new TrainerTrainingsListResponse("New Training",
				LocalDate.of(2024, 2, 1), new TrainingType(1, "cardio"), 2, "trainee");
		TrainerTrainingsListResponse trainerTrainingsListResponse4 = new TrainerTrainingsListResponse("New Training",
				LocalDate.of(2024, 2, 1), new TrainingType(1, "cardio"), 2, "trainee");
		Map<Integer, Map<String, Integer>> yearsList = new HashMap<>();
		Map<String, Integer> monthsList1 = new HashMap<>();
		Map<String, Integer> monthsList2 = new HashMap<>();
		monthsList1.put("JANUARY", 1);
		monthsList1.put("FEBRUARY", 1);
		monthsList2.put("FEBRUARY", 2);
		yearsList.put(LocalDate.now().getYear(), monthsList1);
		yearsList.put(2024, monthsList2);
		TrainerDetailsResponse trainerDetailsResponse = new TrainerDetailsResponse("email@email.com", "firstname",
				"lastname", true, List.of(trainerTrainingsListResponse1, trainerTrainingsListResponse2, trainerTrainingsListResponse3, trainerTrainingsListResponse4));
		SummaryReport summaryReport = new SummaryReport("email@email.com", "firstname", "lastname", true, yearsList);
		when(summaryReportRepository.save(any(SummaryReport.class))).thenReturn(summaryReport);
		when(modelMapper.map(any(SummaryReport.class), eq(SummaryReportDTO.class))).thenReturn(new SummaryReportDTO());
		summaryReportService.createReport(trainerDetailsResponse);
		verify(modelMapper).map(any(SummaryReport.class), eq(SummaryReportDTO.class));
		verify(summaryReportRepository).save(summaryReport);
	}
}
