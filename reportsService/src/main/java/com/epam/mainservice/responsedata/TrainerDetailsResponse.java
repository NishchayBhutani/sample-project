package com.epam.mainservice.responsedata;

import java.util.List;

import com.epam.reportsservice.dto.TrainerTrainingsListResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TrainerDetailsResponse {
	private String email;
	private String firstName;
	private String lastName;
	private boolean isActive;
	private List<TrainerTrainingsListResponse> trainerTrainingsList;
}
