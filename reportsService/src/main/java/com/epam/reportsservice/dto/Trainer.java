package com.epam.reportsservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Trainer {
	private int id;
	private User user;
	private TrainingType trainingType;
	private List<Trainee> traineeList;
	private List<Training> trainingsList;
}
