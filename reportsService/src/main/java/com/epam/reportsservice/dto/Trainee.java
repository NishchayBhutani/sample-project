package com.epam.reportsservice.dto;

import java.time.LocalDate;
import java.util.List;

import lombok.Data;

@Data
public class Trainee {
	private int id;
	private User user;
	private LocalDate dob;
	private String address;
	private List<Trainer> trainersList;
	private List<Training> trainingsList;
}
