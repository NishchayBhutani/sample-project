package com.epam.reportsservice.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TrainerTrainingsListResponse {
	private String trainingName;
	private LocalDate trainingDate;
	private TrainingType trainingType;
	private int trainingDuration;
	private String traineeName;
}
