package com.epam.reportsservice.dto;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SummaryReportDTO {
	private String trainerEmail;
	private String trainerFirstName;
	private String trainerLastName;
	private boolean trainerStatus;
	private Map<Integer, Map<String, Integer>> yearsList;
}