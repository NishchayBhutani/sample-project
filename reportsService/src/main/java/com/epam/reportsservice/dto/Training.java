package com.epam.reportsservice.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class Training {
	private int id;
	private Trainee trainee;
	private Trainer trainer;
	private String trainingName;
    private TrainingType trainingType;
	private LocalDate trainingDate;
	private int trainingDuration;
}