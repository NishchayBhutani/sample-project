package com.epam.reportsservice.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

	@Data
	@AllArgsConstructor
	public class TrainerProfileResponse {
		private String firstName;
		private String lastName;
		private TrainingType trainingType;
		private boolean isActive;
		private List<TraineeInfo> traineesList;	
	}
