package com.epam.reportsservice.dao;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class SummaryReport {
	@Id
	private String trainerEmail;
	private String trainerFirstName;
	private String trainerLastName;
	private boolean trainerStatus;
	private Map<Integer, Map<String, Integer>> yearsList;
}