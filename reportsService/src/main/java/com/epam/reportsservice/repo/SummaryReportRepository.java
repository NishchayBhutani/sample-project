package com.epam.reportsservice.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.epam.reportsservice.dao.SummaryReport;

public interface SummaryReportRepository extends MongoRepository<SummaryReport, String> {

}
