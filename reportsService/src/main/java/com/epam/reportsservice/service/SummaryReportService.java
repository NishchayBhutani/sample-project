package com.epam.reportsservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.mainservice.responsedata.TrainerDetailsResponse;
import com.epam.reportsservice.dao.SummaryReport;
import com.epam.reportsservice.dto.SummaryReportDTO;
import com.epam.reportsservice.dto.TrainerTrainingsListResponse;
import com.epam.reportsservice.repo.SummaryReportRepository;

@Service
public class SummaryReportService {

	@Autowired
	SummaryReportRepository summaryReportRepository;

	@Autowired
	ModelMapper modelMapper;

	public SummaryReportDTO createReport(TrainerDetailsResponse trainerDetailsResponse) {
		List<TrainerTrainingsListResponse> trainingsList = trainerDetailsResponse.getTrainerTrainingsList();
		Map<Integer, Map<String, Integer>> yearsList = new HashMap<>();
		for (TrainerTrainingsListResponse training : trainingsList) {
			if (yearsList.containsKey(training.getTrainingDate().getYear())) {
				Map<String, Integer> monthsList = yearsList.get(training.getTrainingDate().getYear());
				if (monthsList.containsKey(training.getTrainingDate().getMonth().toString())) {
					monthsList.put(training.getTrainingDate().getMonth().toString(),
							monthsList.get(training.getTrainingDate().getMonth().toString()) + 1);
				} else {
					monthsList.put(training.getTrainingDate().getMonth().toString().toString(), 1);
				}
			} else {
				Map<String, Integer> monthsList = new HashMap<>();
				monthsList.put(training.getTrainingDate().getMonth().toString(), 1);
				yearsList.put(training.getTrainingDate().getYear(), monthsList);
			}
		}
		SummaryReport summaryReport = SummaryReport.builder().trainerEmail(trainerDetailsResponse.getEmail())
				.yearsList(yearsList).trainerFirstName(trainerDetailsResponse.getFirstName())
				.trainerLastName(trainerDetailsResponse.getLastName()).trainerStatus(trainerDetailsResponse.isActive())
				.yearsList(yearsList).build();
		return modelMapper.map(summaryReportRepository.save(summaryReport), SummaryReportDTO.class);
	}

}
