package com.epam.reportsservice.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.mainservice.responsedata.TrainerDetailsResponse;
import com.epam.reportsservice.service.SummaryReportService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TrainerDetailsConsumer {

	@Autowired
	SummaryReportService summaryReportService;

	@KafkaListener(topics = "trainer-details", groupId = "trainer-details-consumer-group")
	public void consumer(TrainerDetailsResponse trainerDetailsResponse) {
		log.info("trainer details recieved : " + trainerDetailsResponse);
		summaryReportService.createReport(trainerDetailsResponse);
	}
}
